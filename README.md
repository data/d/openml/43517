# OpenML dataset: Data-Science-YouTube-channels-Video-Metadata

https://www.openml.org/d/43517

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
This dataset contains meta data of around 60 Data Science YouTube channel videos meta data.
Acknowledgements
Data scraped from https://wiki.digitalmethods.net/Dmi/ToolDatabase .
Cover Photo: Photo by Rachit Tank on Unsplash.
Motivation :  Dataset by Gabriel Preda
Inspiration
Possible uses for this dataset could include:

Sentiment analysis/
Categorising YouTube videos based on their comments and statistics.
Training ML algorithms like RNNs to generate their own YouTube description.
Most popular data science youtube channel based on total likes, dislikes, votes counts.
Statistical analysis over different channels_x000C_.

Feel free to check notebook for other possible uses.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43517) of an [OpenML dataset](https://www.openml.org/d/43517). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43517/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43517/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43517/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

